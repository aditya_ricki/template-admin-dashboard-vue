import axios from 'axios';

export default class ProductService {

	getUserLogin(token) {
		return axios.get('http://localhost:8000/api/user', {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.data.data);
    }

	getAllData(token) {
		return axios.get('http://localhost:8000/api/get_all_user', {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.data.data);
    }

    createData(token, data) {
		return axios.post(`http://localhost:8000/api/user`, data, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.data.data);
    }

    updateData(token, id, data) {
		return axios.put(`http://localhost:8000/api/user/${id}`, data, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.data.data);
    }

    deleteData(token, id) {
		return axios.delete(`http://localhost:8000/api/user/${id}`, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.data.data);
    }
}
