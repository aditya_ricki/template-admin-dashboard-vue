import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from './components/Dashboard.vue';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'dashboard',
			component: Dashboard
		},

		// test
		{
			path: '/login',
			name: 'login',
			component: () => import('./pages/auth/Index.vue')
		},

		// test
		{
			path: '/user',
			name: 'user',
			component: () => import('./pages/user/Index.vue')
		},
	],
    mode: 'history',
	scrollBehavior() {
		return {x: 0, y: 0};
	}
});
